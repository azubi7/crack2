#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;   
const int HASH_LEN=33;

char *crackHash(char *target, char *dictionary)
{
    FILE *df = fopen(dictionary, "r");
    if (df == NULL)
    {
        perror("Can't open given file");
        exit(1);
    }
    char *word = malloc(sizeof(char)*PASS_LEN);
    while(fgets(word, PASS_LEN, df) != NULL)
    {
        char *nl = strchr(word, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        char *hash = md5(word, strlen(word));
        if(strcmp(hash, target)==0)
        {
            free(hash);
            return word;
        }
    }
    fclose(df);
}
int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    FILE *hfile = fopen(argv[1], "r");
    if (hfile == NULL)
    {
        perror("Can't open argv[1]");
        exit(1);
    }
    char line[50];
    while(fgets(line, 50, hfile) != NULL)
    {
        char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        char *output = crackHash(line, argv[2]);
        printf("%s %s\n", line, output);
        free(output);
    }

    fclose(hfile);
    return 0;

}
